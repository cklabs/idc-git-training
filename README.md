# IDC Git Training Repository

# Install Git
- Linux [Debian] - `sudo apt install git`
- Linux [Red Hat / Oracle] - `sudo yum install git`
- [Windows](https://git-scm.com/download/win)
- [Mac](https://git-scm.com/download/mac)

# Git Exercise 1
- Fork this repository by clicking the 'Fork' button
- Clone your fork
- Create a new file and name it \<your name\>.txt
- Write a brief introduction about yourself in that file
- Add the file to git
- Commit the file. In the commit message, write "Created introduction for \<your name\>"
- Push the file to origin master
- Open a Merge Request to this Repository

# Next Activity
Once this activity has been completed, please go to: [Jenkins Activity](https://gitlab.com/abhiramk/idc-jenkins-activity)